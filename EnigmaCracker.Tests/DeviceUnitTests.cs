using EnigmaCracker.Analysis;
using EnigmaCracker.Device;
using NUnit.Framework;
using System;

namespace EnigmaCracker.Tests {
    public class DeviceUnitTests {

        [Test]
        [TestCase("ABCDEFGHIJKLMNOPQRSTUVWXYZAAAAAAAAAAAAAAAAAAAAAAAAAABBBBBBBBBBBBBBBBBBBBBBBBBBABCDEFGHIJKLMNOPQRSTUVWXYZ", "BJELRQZVJWARXSNBXORSTNCFMEYHCXTGYJFLINHNXSHIUNTHEORXOPLOVFEKAGADSPNPCMHRVZCYECDAZIHVYGPITMSRZKGGHLSRBLHL", new string[] { "I", "II", "III" }, "B", new int[] { 0, 0, 0 }, new int[] { 0, 0, 0 }, "")]
        [TestCase("ABCDEFGHIJKLMNOPQRSTUVWXYZAAAAAAAAAAAAAAAAAAAAAAAAAABBBBBBBBBBBBBBBBBBBBBBBBBBABCDEFGHIJKLMNOPQRSTUVWXYZ", "FOTYBPKLBZQSGZBOPUFYPFUSETWKNQQHVNHLKJZZZKHUBEJLGVUNIOYSDTEZJQHHAOYYZSENTGXNJCHEDFHQUCGCGJBURNSEDZSEPLQP", new string[] { "VII", "V", "IV" }, "B", new int[] { 10, 5, 12 }, new int[] { 1, 2, 3 }, "")]
        [TestCase("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "YJKJMFQKPCUOCKTEZQVXYZJWJFROVJMWJVXRCQYFCUVBRELVHRWGPYGCHVLBVJEVTTYVMWKJFOZHLJEXYXRDBEVEHVXKQSBPYZNIQDCBGTDDWZQWLHIBQNTYPIEBMNINNGMUPPGLSZCBRJULOLNJSOEDLOBXXGEVTKCOTTLDZPHBUFKLWSFSRKOMXKZELBDJNRUDUCOTNCGLIKVKMHHCYDEKFNOECFBWRIEFQQUFXKKGNTSTVHVITVHDFKIJIHOGMDSQUFMZCGGFZMJUKGDNDSNSJKWKENIRQKSUUHJYMIGWWNMIESFRCVIBFSOUCLBYEEHMESHSGFDESQZJLTORNFBIFUWIFJTOPVMFQCFCFPYZOJFQRFQZTTTOECTDOOYTGVKEWPSZGHCTQRPGZQOVTTOIEGGHEFDOVSUQLLGNOOWGLCLOWSISUGSVIHWCMSIUUSBWQIGWEWRKQFQQRZHMQJNKQTJFDIJYHDFCWTHXUOOCVRCVYOHL", new string[] { "III", "VI", "VIII" }, "B", new int[] { 3, 5, 9 }, new int[] { 11, 13, 19 }, "")]
        [TestCase("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "QREBNMCYZELKQOJCGJVIVGLYEMUPCURPVPUMDIWXPPWROOQEGI", new string[] { "I", "II", "III" }, "B", new int[] { 0, 0, 0 }, new int[] { 0, 0, 0 }, "AC FG JY LW")]
        [TestCase("WRBHFRROSFHBCHVBENQFAGNYCGCRSTQYAJNROJAKVKXAHGUZHZVKWUTDGMBMSCYQSKABUGRVMIUOWAPKCMHYCRTSDEYTNJLVWNQY", "FYTIDQIBHDONUPAUVPNKILDHDJGCWFVMJUFNJSFYZTSPITBURMCJEEAMZAZIJMZAVFCTYTKYORHYDDSXHBLQWPJBMSSWIPSWLENZ", new string[] { "IV", "VI", "III" }, "B", new int[] { 0, 10, 6 }, new int[] { 0, 0, 0 }, "BM DH RS KN GZ FQ")]
        [TestCase("RNXYAZUYTFNQFMBOLNYNYBUYPMWJUQSBYRHPOIRKQSIKBKEKEAJUNNVGUQDODVFQZHASHMQIHSQXICTSJNAUVZYIHVBBARPJADRH", "CFBJTPYXROYGGVTGBUTEBURBXNUZGGRALBNXIQHVBFWPLZQSCEZWTAWCKKPRSWOGNYXLCOTQAWDRRKBCADTKZGPWSTNYIJGLVIUQ", new string[] { "I", "II", "III" }, "B", new int[] { 0, 1, 20 }, new int[] { 5, 5, 4 }, "AG HR YT KI FL WE NM SD OP QJ")]
        public void Encryption_EnigmaSettingKnown_InputKnown(string plaintext, string cyphertext, string[] rotors, string reflector, int[] positions, int[] ringSettings, string plugboard) {
            Enigma enigmaMachine = new(rotors, reflector, positions, ringSettings, plugboard);
            char[] ciphertextTest = enigmaMachine.Encrypt(plaintext.ToCharArray());
            Assert.AreEqual(new string(ciphertextTest), cyphertext);
        }


        [Test]
        [TestCase(10, 1000)]
        public void Decryption_RandomizedEnigmaSettings_RandomizedInput(int numberOfTests, int numberOfChar) {
            Random rand = new();
            string[] availableRotors = new[]{ "I", "II", "III", "IV", "V", "VI", "VII", "VIII" };

            char[] input = new char[numberOfChar];
            for (int i = 0; i < numberOfChar; i++)
                input[i] = (char)(rand.Next(AnalysisConstants.LettersCount) + 65);

            for (int test = 0; test < numberOfTests; test++) {
                string[] rotors = new[]{ 
                    availableRotors[rand.Next(availableRotors.Length)],
                    availableRotors[rand.Next(availableRotors.Length)],
                    availableRotors[rand.Next(availableRotors.Length)]
                };

                int[] startingPositions = new[]{ 
                    rand.Next(AnalysisConstants.LettersCount), 
                    rand.Next(AnalysisConstants.LettersCount), 
                    rand.Next(AnalysisConstants.LettersCount) 
                };

                int[] ringSettings = new[]{ 
                    rand.Next(AnalysisConstants.LettersCount), 
                    rand.Next(AnalysisConstants.LettersCount), 
                    rand.Next(AnalysisConstants.LettersCount) 
                };

                // Encryption
                Enigma enigmaEncrypt = new(rotors, "B", startingPositions, ringSettings, "");
                char[] ciphertext = enigmaEncrypt.Encrypt(input);

                // Decryption
                Enigma enigmaDecrypt = new(rotors, "B", startingPositions, ringSettings, "");
                char[] plaintext = enigmaDecrypt.Encrypt(ciphertext);

                Assert.AreEqual(input, plaintext);
            }
        }
    }
}