﻿using System.Reflection;
using System.Text;

namespace EnigmaCracker.Utils {
    public class EmbededResourceReader {
        protected StreamReader LoadStream(string fileName) {
            Assembly assembly = GetType().Assembly;
            Stream resourceStream = assembly.GetManifestResourceStream($"{GetType().Namespace}.{fileName}");
            return new StreamReader(resourceStream, Encoding.UTF8);
        }
    }
}
