﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnigmaCracker.Utils {
    public class Utility {
        public static int MathMod(int a, int b) => (Math.Abs(a * b) + a) % b;
    }
}
