﻿using EnigmaCracker.Analysis;

namespace EnigmaCracker.Device {
    public class Enigma {

        public Rotor LeftRotor;
        public Rotor MiddleRotor;
        public Rotor RightRotor;
        public Reflector Reflector;
        public Plugboard Plugboard;

        public Enigma(EnigmaKey key) : this(key.Rotors, "B", key.Indicators, key.Rings, key.Plugboard) { }
        public Enigma(string[] rotors, string reflector, int[] rotorPositions, int[] ringSettings, string plugboardConnections) {
            LeftRotor = Rotor.Create(rotors[0], rotorPositions[0], ringSettings[0]);
            MiddleRotor = Rotor.Create(rotors[1], rotorPositions[1], ringSettings[1]);
            RightRotor = Rotor.Create(rotors[2], rotorPositions[2], ringSettings[2]);
            Reflector = Reflector.Create(reflector);
            Plugboard = new Plugboard(plugboardConnections);
        }

        public void Rotate() {
            // If middle rotor notch - double-stepping
            if (MiddleRotor.IsAtNotch()) {
                MiddleRotor.Turnover();
                LeftRotor.Turnover();
            }
            // If left-rotor notch
            else if (RightRotor.IsAtNotch())
                MiddleRotor.Turnover();

            // Increment right-most rotor
            RightRotor.Turnover();
        }

        public int Encrypt(int c) {
            Rotate();

            // Plugboard in
            c = Plugboard.Forward(c);

            // Right to left
            int c1 = RightRotor.Forward(c);
            int c2 = MiddleRotor.Forward(c1);
            int c3 = LeftRotor.Forward(c2);

            // Reflector
            int c4 = Reflector.Forward(c3);

            // Left to right
            int c5 = LeftRotor.Backward(c4);
            int c6 = MiddleRotor.Backward(c5);
            int c7 = RightRotor.Backward(c6);

            // Plugboard out
            c7 = Plugboard.Forward(c7);

            return c7;
        }

        private char Encrypt(char c) => (char)(Encrypt(c - 65) + 65);

        public char[] Encrypt(char[] input) {
            char[] output = new char[input.Length];
            for (int i = 0; i < input.Length; i++)
                output[i] = Encrypt(input[i]);
            return output;
        }

        public void ResetRotorPosition(int leftPosition, int middlePosition, int rightPosition) {
            LeftRotor.RotorPosition = leftPosition;
            MiddleRotor.RotorPosition = middlePosition;
            RightRotor.RotorPosition = rightPosition;
        }
    }
}
