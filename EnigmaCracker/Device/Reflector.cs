﻿using EnigmaCracker.Utils;

namespace EnigmaCracker.Device {
    public class Reflector {

        private readonly int[] ForwardWiring;

        private Reflector(string encoding) => ForwardWiring = DecodeWiring(encoding);

        public static Reflector Create(string name) 
            => DeviceConstants.ReflectorConfigurations.ContainsKey(name) ? new Reflector(DeviceConstants.ReflectorConfigurations[name]) : new Reflector("ZYXWVUTSRQPONMLKJIHGFEDCBA");

        private static int[] DecodeWiring(string encoding) {
            char[] charWiring = encoding.ToCharArray();
            int[] wiring = new int[charWiring.Length];
            for (int i = 0; i < charWiring.Length; i++)
                wiring[i] = charWiring[i] - 65;
            return wiring;
        }

        public int Forward(int c) => ForwardWiring[c];
    }
}
