﻿using EnigmaCracker.Analysis;
using System.Text.RegularExpressions;

namespace EnigmaCracker.Device {
    public class Plugboard {

        private readonly int[] Wiring;

        public Plugboard(string connections) => 
            Wiring = DecodePlugboard(connections);
        
        public int Forward(int c) => Wiring[c];

        private static int[] IdentityPlugboard() {
            int[] mapping = new int[AnalysisConstants.LettersCount];

            for (int i = 0; i < AnalysisConstants.LettersCount; i++)
                mapping[i] = i;
            return mapping;
        }

        public static HashSet<int> GetUnpluggedCharacters(string plugboard) {
            HashSet<int> unpluggedCharacters = new();

            for (int i = 0; i < AnalysisConstants.LettersCount; i++)
                unpluggedCharacters.Add(i);

            if (string.IsNullOrEmpty(plugboard))
                return unpluggedCharacters;

            string[] pairings = Regex.Split(plugboard, @"[^a-zA-Z]");

            // Validate and create mapping
            foreach (string pair in pairings) {
                int c1 = pair[0] - 65;
                int c2 = pair[1] - 65;

                unpluggedCharacters.Remove(c1);
                unpluggedCharacters.Remove(c2);
            }
            return unpluggedCharacters;
        }

        private static int[] DecodePlugboard(string plugboard) {
            if (string.IsNullOrEmpty(plugboard))
                return IdentityPlugboard();

            string[] pairings = Regex.Split(plugboard, @"[^a-zA-Z]");
            HashSet<int> pluggedCharacters = new();
            int[] mapping = IdentityPlugboard();

            // Validate and create mapping
            foreach(string pair in pairings) {
                if (pair.Length != 2)
                    return IdentityPlugboard();

                int c1 = pair[0] - 65;
                int c2 = pair[1] - 65;

                if (pluggedCharacters.Contains(c1) || pluggedCharacters.Contains(c2))
                    return IdentityPlugboard();

                pluggedCharacters.Add(c1);
                pluggedCharacters.Add(c2);

                mapping[c1] = c2;
                mapping[c2] = c1;
            }
            return mapping;
        }
    }
}
