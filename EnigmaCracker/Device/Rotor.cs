﻿using EnigmaCracker.Analysis;

namespace EnigmaCracker.Device {
    public class Rotor {

        private string _name;
        public string Name {
            get => _name;
            set => _name = value;
        }

        private int _rotorPosition;
        public int RotorPosition {
            get => _rotorPosition;
            set => _rotorPosition = value;
        }

        private bool _rotorAboveFive = false;
        public bool RotorAboveFive {
            get => _rotorAboveFive;
            set => _rotorAboveFive = value;
        }

        private readonly int[] ForwardWiring;
        private readonly int[] BackwardWiring;
        private readonly int NotchPosition;
        private readonly int RingSetting;

        public Rotor(string name, string encoding, int rotorPosition, int notchPosition, int ringSetting, bool rotorAboveFive = false) {
            Name = name;
            ForwardWiring = DecodeWiring(encoding);
            BackwardWiring = InverseWiring(ForwardWiring);
            RotorPosition = rotorPosition;
            NotchPosition = notchPosition;
            RingSetting = ringSetting;
            RotorAboveFive = rotorAboveFive;
        }

        public static Rotor Create(string name, int rotorPosition, int ringSetting) {
            if (!DeviceConstants.RotorConfigurations.ContainsKey(name))
                name = "Default";
            var rotorType = DeviceConstants.RotorConfigurations[name];
            if (rotorType.Item1 == "VI" || rotorType.Item1 == "VII" || rotorType.Item1 == "VIII")
                return new Rotor(rotorType.Item1, rotorType.Item2, rotorPosition, rotorType.Item3, ringSetting, true);
            return new Rotor(rotorType.Item1, rotorType.Item2, rotorPosition, rotorType.Item3, ringSetting);
        }

        private static int[] DecodeWiring(string encoding) {
            char[] charWiring = encoding.ToCharArray();
            int[] wiring = new int[charWiring.Length];
            for (int i = 0; i < charWiring.Length; i++)
                wiring[i] = charWiring[i] - 65;
            return wiring;
        }

        private static int[] InverseWiring(int[] wiring) {
            int[] inverse = new int[wiring.Length];
            for (int i = 0; i < wiring.Length; i++) {
                int forward = wiring[i];
                inverse[forward] = i;
            }
            return inverse;
        }

        private static int Encipher(int k, int pos, int ring, int[] mapping) {
            int shift = pos - ring;
            return (mapping[(k + shift + AnalysisConstants.LettersCount) % AnalysisConstants.LettersCount] - shift + AnalysisConstants.LettersCount) % AnalysisConstants.LettersCount;
        }

        public int Forward(int c) => Encipher(c, RotorPosition, RingSetting, ForwardWiring);
        public int Backward(int c) => Encipher(c, RotorPosition, RingSetting, BackwardWiring);
        public bool IsAtNotch() => RotorAboveFive ? RotorPosition == 12 || RotorPosition == 25 : NotchPosition == RotorPosition;
        public void Turnover() => RotorPosition = (RotorPosition + 1) % AnalysisConstants.LettersCount;
    }
}
