﻿
namespace EnigmaCracker.Device {
    public class DeviceConstants {

        public static readonly Dictionary<string, (string, string, int)> RotorConfigurations = new() {
            { "I", ("I", "EKMFLGDQVZNTOWYHXUSPAIBRCJ", 16) },
            { "II", ("II", "AJDKSIRUXBLHWTMCQGZNPYFVOE", 4) },
            { "III", ("III", "BDFHJLCPRTXVZNYEIWGAKMUSQO", 21) },
            { "IV", ("IV", "ESOVPZJAYQUIRHXLNFTGKDCMWB", 9) },
            { "V", ("V", "VZBRGITYUPSDNHLXAWMJQOFECK", 25) },
            { "VI", ("VI", "JPGVOUMFYQBENHZRDKASXLICTW", 0) },
            { "VII", ("VII", "NZJHGRCXMYSWBOUFAIVLPEKQDT", 0) },
            { "VIII", ("VIII", "FKQHTLXOCBJSPDZRAMEWNIUYGV", 0) },
            { "Default", ("Identity", "ABCDEFGHIJKLMNOPQRSTUVWXYZ", 0) }
        };

        public static readonly Dictionary<string, string> ReflectorConfigurations = new() {
            { "B", "YRUHQSLDPXNGOKMIEBFZCWVJAT" },
            { "C", "FVPJIAOYEDRZXWGCTKUQSBNMHL" }
        };

    }
}
