﻿
namespace EnigmaCracker.Analysis.Common {
    public class Helper {

        public static List<string[]> GetRotorPermutations(AnalysisConstants.AvailableRotors rotors) {
            string[] availableRotorList = AnalysisConstants.Rotors[rotors];
            List<string[]> result = new();
            foreach (string rotor1 in availableRotorList) {
                foreach (string rotor2 in availableRotorList) {
                    if (rotor1 == rotor2)
                        continue;
                    foreach (string rotor3 in availableRotorList) {
                        if (rotor1 == rotor3 || rotor2 == rotor3)
                            continue;
                        result.Add(new[]{ rotor1, rotor2, rotor3 });
                    }
                }
            }
            return result;
        }

    }
}
