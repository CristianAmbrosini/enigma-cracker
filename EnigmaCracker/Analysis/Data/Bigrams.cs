﻿using EnigmaCracker.Utils;

namespace EnigmaCracker.Analysis.Data {
    public class Bigrams : EmbededResourceReader {
        public StreamReader LoadStream() => LoadStream($"{nameof(Bigrams)}.txt");
    }
}
