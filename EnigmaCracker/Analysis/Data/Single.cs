﻿using EnigmaCracker.Utils;

namespace EnigmaCracker.Analysis.Data {
    public class Single : EmbededResourceReader {
        public StreamReader LoadStream() => LoadStream($"{nameof(Single)}.txt");
    }
}
