﻿using EnigmaCracker.Utils;

namespace EnigmaCracker.Analysis.Data {
    public class Trigrams : EmbededResourceReader {
        public StreamReader LoadStream() => LoadStream($"{nameof(Trigrams)}.txt");
    }
}
