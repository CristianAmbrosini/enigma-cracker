﻿using EnigmaCracker.Utils;

namespace EnigmaCracker.Analysis.Data {
    public class Quadgrams : EmbededResourceReader {
        public StreamReader LoadStream() => LoadStream($"{nameof(Quadgrams)}.txt");
    }
}
