﻿namespace EnigmaCracker.Analysis {
    public class EnigmaKey {

        public string[] _rotors = new[]{ "I", "II", "III" };
        public string[] Rotors {
            get => _rotors;
            set {
                if (value != null)
                    _rotors = value;
            }
        }

        private int[] _indicators = new[]{ 0, 0, 0 };
        public int[] Indicators {
            get => _indicators;
            set {
                if (value != null)
                    _indicators = value;
            }
        }

        private int[] _rings = new[] { 0, 0, 0 };
        public int[] Rings {
            get => _rings;
            set { 
                if (value != null)
                    _rings = value;
            }
        }

        private string _plugboard = "";
        public string Plugboard {
            get => _plugboard;
            set {
                if (_plugboard != value)
                    _plugboard = value;
            }
        }

        public EnigmaKey(string[] rotors, int[] indicators, int[] rings, String plugboardConnections) {
            Rotors = rotors;
            Indicators = indicators;
            Rings = rings;
            Plugboard = plugboardConnections;
        }

        public EnigmaKey(EnigmaKey k) {
            Rotors = k.Rotors;
            Indicators = k.Indicators;
            Rings = k.Rings;
            Plugboard = k.Plugboard;
        }
    }
}
