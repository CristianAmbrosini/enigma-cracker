﻿namespace EnigmaCracker.Analysis {
    public class AnalysisConstants {

        public const float InitialMaxFitness = -1e30f;
        public const float Epsilon = 3e-10f;
        public const int LettersCount = 26;

        public enum AvailableRotors { THREE, FIVE, EIGHT }
        public static readonly Dictionary<AvailableRotors, string[]> Rotors = new() {
            { AvailableRotors.THREE, new[]{ "I", "II", "III" } },
            { AvailableRotors.FIVE, new[]{ "I", "II", "III", "IV", "V" } },
            { AvailableRotors.EIGHT, new[]{ "I", "II", "III", "IV", "V", "VI", "VII", "VIII" } },
        };
    }
}
