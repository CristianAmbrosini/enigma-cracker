﻿using System.Collections;

namespace EnigmaCracker.Analysis {
    public class ScoredEnigmaKey : EnigmaKey, IComparable<ScoredEnigmaKey> {

        private float _score = 0f;
        public float Score {
            get => _score;
            set => _score = value;
        }

        public ScoredEnigmaKey(EnigmaKey key, float score) : base(key) {
            Score = score;
        }

        public int CompareTo(ScoredEnigmaKey? other) => Score.CompareTo(other.Score);
    }
}
