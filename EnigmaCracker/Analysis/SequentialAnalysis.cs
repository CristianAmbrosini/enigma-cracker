﻿using EnigmaCracker.Analysis.Common;
using EnigmaCracker.Analysis.Fitness;
using EnigmaCracker.Device;
using EnigmaCracker.Utils;

namespace EnigmaCracker.Analysis {
    public class SequentialAnalysis {
        public static ScoredEnigmaKey[] FindRotorConfiguration(char[] ciphertext, string plugboard, int requiredKeys, FitnessFunction f, AnalysisConstants.AvailableRotors rotors = AnalysisConstants.AvailableRotors.EIGHT) {
            string[] optimalRotors;
            int[] optimalPositions;

            List<ScoredEnigmaKey> keySet = new();
            List<string[]> rotorCombinations = Helper.GetRotorPermutations(rotors);
            Console.WriteLine("Evaluating combinations (sequential analysis):");
            foreach(string[] combination in rotorCombinations) {
                Console.WriteLine($"... {combination[0]} {combination[1]} {combination[2]}");
                float maxFitness = AnalysisConstants.InitialMaxFitness;
                Enigma enigma = new(combination, "B", new int[] { 0, 0, 0 }, new int[] { 0, 0, 0 }, plugboard);
                EnigmaKey bestKey = null;
                for (int i = 0; i < AnalysisConstants.LettersCount; i++) {
                    for (int j = 0; j < AnalysisConstants.LettersCount; j++) {
                        for (int k = 0; k < AnalysisConstants.LettersCount; k++) {
                            enigma.ResetRotorPosition(i, j, k);
                            char[] decryption = enigma.Encrypt(ciphertext);
                            float fitness = f.Score(decryption);
                            //Console.WriteLine($"... {combination[0]} {combination[1]} {combination[2]} - {i} {j} {k} | {fitness}");
                            if (fitness > maxFitness) {
                                maxFitness = fitness;
                                optimalRotors = new string[] { enigma.LeftRotor.Name, enigma.MiddleRotor.Name, enigma.RightRotor.Name };
                                optimalPositions = new int[] { i, j, k };
                                bestKey = new EnigmaKey(optimalRotors, optimalPositions, null, plugboard);
                            }
                        }
                    }
                }
                keySet.Add(new ScoredEnigmaKey(bestKey, maxFitness));
            };
            // Sort keys by best performing (highest fitness score)
            return keySet.OrderBy(p => p.Score).Reverse().Take(requiredKeys).ToArray();
        }

        public static ScoredEnigmaKey FindRingSettings(EnigmaKey key, char[] ciphertext, FitnessFunction f) {
            EnigmaKey newKey = new(key);

            int rightRotorIndex = 2, middleRotorIndex = 1;

            // Optimise right rotor
            int optimalIndex = FindRingSetting(newKey, ciphertext, rightRotorIndex, f);
            newKey.Rings[rightRotorIndex] = optimalIndex;
            newKey.Indicators[rightRotorIndex] = (newKey.Indicators[rightRotorIndex] + optimalIndex) % AnalysisConstants.LettersCount;

            // Optimise middle rotor
            optimalIndex = FindRingSetting(newKey, ciphertext, middleRotorIndex, f);
            newKey.Rings[middleRotorIndex] = optimalIndex;
            newKey.Indicators[middleRotorIndex] = (newKey.Indicators[middleRotorIndex] + optimalIndex) % AnalysisConstants.LettersCount;

            // Calculate fitness and return scored key
            Enigma enigma = new(newKey);
            char[] decryption = enigma.Encrypt(ciphertext);
            return new ScoredEnigmaKey(newKey, f.Score(decryption));
        }

        private static int FindRingSetting(EnigmaKey key, char[] ciphertext, int rotor, FitnessFunction f) {
            string[] rotors = key.Rotors;
            int[] originalIndicators = key.Indicators;
            int[] originalRingSettings = key.Rings ?? (new int[] { 0, 0, 0 });
            string plugboard = key.Plugboard;
            int optimalRingSetting = 0;

            float maxFitness = AnalysisConstants.InitialMaxFitness;
            for (int i = 0; i < AnalysisConstants.LettersCount; i++) {
                int[] currentStartingPositions = new int[3];
                Array.Copy(originalIndicators, currentStartingPositions, 3);
                int[] currentRingSettings = new int[3];
                Array.Copy(originalRingSettings, currentRingSettings, 3);

                currentStartingPositions[rotor] = Utility.MathMod(currentStartingPositions[rotor] + i, AnalysisConstants.LettersCount);
                currentRingSettings[rotor] = i;

                Enigma enigma = new (rotors, "B", currentStartingPositions, currentRingSettings, plugboard);
                char[] decryption = enigma.Encrypt(ciphertext);
                float fitness = f.Score(decryption);
                if (fitness > maxFitness) {
                    maxFitness = fitness;
                    optimalRingSetting = i;
                }
            }
            return optimalRingSetting;
        }

        private static string FindPlug(EnigmaKey key, char[] ciphertext, FitnessFunction f) {
            HashSet<int> unpluggedCharacters = Plugboard.GetUnpluggedCharacters(key.Plugboard);
            EnigmaKey currentKey = new(key);
            string originalPlugs = currentKey.Plugboard;
            string optimalPlugSetting = "";
            float maxFitness = AnalysisConstants.InitialMaxFitness;
            foreach(int i in unpluggedCharacters) {
                foreach(int j in unpluggedCharacters) {
                    if (i >= j) 
                        continue;
                    string plug = "" + (char)(i + 65) + (char)(j + 65);
                    currentKey.Plugboard = string.IsNullOrEmpty(originalPlugs) ? plug : originalPlugs + " " + plug;

                    Enigma enigma = new(currentKey);
                    char[] decryption = enigma.Encrypt(ciphertext);
                    float fitness = f.Score(decryption);
                    if (fitness > maxFitness) {
                        maxFitness = fitness;
                        optimalPlugSetting = plug;
                    }
                }
            }
            return optimalPlugSetting;
        }

        public static ScoredEnigmaKey FindPlugs(EnigmaKey key, int maxPlugs, char[] ciphertext, FitnessFunction f) {
            EnigmaKey currentKey = new(key);
            string plugs = "";
            for (int i = 0; i < maxPlugs; i++) {
                currentKey.Plugboard = plugs;
                string nextPlug = FindPlug(currentKey, ciphertext, f);
                plugs = string.IsNullOrEmpty(plugs) ? nextPlug : plugs + " " + nextPlug;
            }
            currentKey.Plugboard = plugs;

            // Calculate fitness and return scored key
            Enigma enigma = new(currentKey);
            char[] decryption = enigma.Encrypt(ciphertext);
            return new ScoredEnigmaKey(currentKey, f.Score(decryption));
        }
    }
}
