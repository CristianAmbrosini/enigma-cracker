﻿namespace EnigmaCracker.Analysis.Fitness {
    public class FrequencyAnalysis {

        private readonly float[] Counts = new float[AnalysisConstants.LettersCount];
        private int Total = 0;

        public void Analyse(byte[] text) {
            foreach(byte b in text)
                Counts[b]++;
            Total += text.Length;
        }

        public float[] Frequencies() {
            float[] freq = new float[AnalysisConstants.LettersCount];
            for (int i = 0; i < freq.Length; i++)
                freq[i] = Counts[i] / Total;
            return freq;
        }
    }
}
