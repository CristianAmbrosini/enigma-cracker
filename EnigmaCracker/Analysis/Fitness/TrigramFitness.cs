﻿
namespace EnigmaCracker.Analysis.Fitness {
    public class TrigramFitness : FitnessFunction {

        private readonly float[] Trigrams = new float[26426];

        private static int TriIndex(int a, int b, int c) => (a << 10) | (b << 5) | c;

        public TrigramFitness() {
            Array.Fill(Trigrams, (float)Math.Log10(AnalysisConstants.Epsilon));
            
            string line;
            using StreamReader reader = new Data.Trigrams().LoadStream();
            while ((line = reader.ReadLine()) != null) {
                string[] parts = line.Split(',');
                string key = parts[0];
                int i = TriIndex(key[0] - 65, key[1] - 65, key[2] - 65);
                Trigrams[i] = float.Parse(parts[1]);
            }
        }

        public override float Score(char[] text) {
            float fitness = 0;
            int current;
            int next1 = text[0] - 65;
            int next2 = text[1] - 65;
            for (int i = 2; i < text.Length; i++) {
                current = next1;
                next1 = next2;
                next2 = text[i] - 65;
                fitness += Trigrams[TriIndex(current, next1, next2)];
            }
            return fitness;
        }
    }
}
