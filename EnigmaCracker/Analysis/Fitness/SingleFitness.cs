﻿
namespace EnigmaCracker.Analysis.Fitness {
    public class SingleFitness : FitnessFunction {

        private readonly float[] Singles = new float[AnalysisConstants.LettersCount];

        public SingleFitness() {
            Array.Fill(Singles, (float)Math.Log10(AnalysisConstants.Epsilon));
            string line;
            using StreamReader reader = new Data.Single().LoadStream();
            while ((line = reader.ReadLine()) != null) {
                string[] parts = line.Split(',');
                string key = parts[0];
                int i = key[0] - 65;
                Singles[i] = float.Parse(parts[1]);
            }
        }

        public override float Score(char[] text) {
            float fitness = 0;
            foreach(char c in text)
                fitness += Singles[c - 65];
            return fitness;
        }
    }
}
