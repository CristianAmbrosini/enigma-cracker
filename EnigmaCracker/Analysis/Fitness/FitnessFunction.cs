﻿
namespace EnigmaCracker.Analysis.Fitness {
    public abstract class FitnessFunction {
        public virtual float Score(char[] text) => 0f;
    }
}
