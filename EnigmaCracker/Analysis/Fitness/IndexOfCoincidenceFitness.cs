﻿namespace EnigmaCracker.Analysis.Fitness {
    public class IndexOfCoincidenceFitness : FitnessFunction {
        public override float Score(char[] text) {
            int[] histogram = new int[AnalysisConstants.LettersCount];
            foreach(char c in text)
                histogram[c - 65]++;

            int n = text.Length;
            float total = 0.0f;

            foreach(int v in histogram)
                total += (v * (v - 1));

            return total / (n * (n - 1));
        }
    }
}
