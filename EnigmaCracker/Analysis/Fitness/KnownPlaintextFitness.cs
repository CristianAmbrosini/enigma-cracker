﻿namespace EnigmaCracker.Analysis.Fitness {
    public class KnownPlaintextFitness : FitnessFunction {

        private readonly char[] Plaintext;

        public KnownPlaintextFitness(char[] plaintext) => Plaintext = plaintext;
        public KnownPlaintextFitness(string[] words, int[] offsets) {
            int length = 0;
            for (int i = 0; i < words.Length; i++) {
                int offset = offsets[i] + words[i].Length;
                length = Math.Max(offset, length);
            }

            Plaintext = new char[length];
            for (int i = 0; i < words.Length; i++)
                Array.Copy(words[i].ToCharArray(), 0, Plaintext, offsets[i], words[i].Length);
        }

        public override float Score(char[] text) {
            int length = Math.Min(Plaintext.Length, text.Length);
            int total = 0;
            for (int i = 0; i < length; i++)
                if (Plaintext[i] > 0)
                    total += Plaintext[i] == text[i] ? 1 : 0;
            return total;
        }
    }
}
