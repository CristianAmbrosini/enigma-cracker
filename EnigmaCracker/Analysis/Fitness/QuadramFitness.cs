﻿
namespace EnigmaCracker.Analysis.Fitness {
    public class QuadramFitness : FitnessFunction {

        private readonly float[] Quadrams = new float[845626];

        private static int QuadIndex(int a, int b, int c, int d) => (a << 15) | (b << 10) | (c << 5) | d;

        public QuadramFitness() {
            Array.Fill(Quadrams, (float)Math.Log10(AnalysisConstants.Epsilon));
            
            string line;
            using StreamReader reader = new Data.Quadgrams().LoadStream();
            while ((line = reader.ReadLine()) != null) {
                string[] parts = line.Split(",");
                string key = parts[0];
                int i = QuadIndex(key[0] - 65, key[1] - 65, key[2] - 65, key[3] - 65);
                Quadrams[i] = float.Parse(parts[1]);
            }
        }

        public override float Score(char[] text) {
            float fitness = 0;
            int current;
            int next1 = text[0] - 65;
            int next2 = text[1] - 65;
            int next3 = text[2] - 65;
            for (int i = 3; i < text.Length; i++) {
                current = next1;
                next1 = next2;
                next2 = next3;
                next3 = text[i] - 65;
                fitness += Quadrams[QuadIndex(current, next1, next2, next3)];
            }
            return fitness;
        }
    }
}
