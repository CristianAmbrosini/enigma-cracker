﻿namespace EnigmaCracker.Analysis.Fitness {
    public class BigramFitness : FitnessFunction {

        private readonly float[] Bigrams = new float[826];

        private static int BiIndex(int a, int b) => (a << 5) | b;

        public BigramFitness() {
            Array.Fill(Bigrams, (float)Math.Log10(AnalysisConstants.Epsilon));
            
            string line;
            using StreamReader reader = new Data.Bigrams().LoadStream();
            while ((line = reader.ReadLine()) != null) {
                string[] parts = line.Split(",");
                string key = parts[0];
                int i = BiIndex(key[0] - 65, key[1] - 65);
                Bigrams[i] = float.Parse(parts[1]);
            }
        }
 
        public override float Score(char[] text) {
            float fitness = 0;
            int current;
            int next = text[0] - 65;
            for (int i = 1; i < text.Length; i++) {
                current = next;
                next = text[i] - 65;
                int b = BiIndex(current, next);
                fitness += Bigrams[b];
            }
            return fitness;
        }
    }
}
