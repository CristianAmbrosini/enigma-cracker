﻿using EnigmaCracker.Analysis;
using EnigmaCracker.Analysis.Fitness;
using EnigmaCracker.Device;
using System.Diagnostics;
using System.Text.RegularExpressions;

// II V III / 7 4 19 / 12 2 20 / AF TV KO BL RW
// "OZLUDYAKMGMXVFVARPMJIKVWPMBVWMOIDHYPLAYUWGBZFAFAFUQFZQISLEZMYPVBRDDLAGIHIFUJDFADORQOOMIZPYXDCBPWDSSNUSYZTJEWZPWFBWBMIEQXRFASZLOPPZRJKJSPPSTXKPUWYSKNMZZLHJDXJMMMDFODIHUBVCXMNICNYQBNQODFQLOGPZYXRJMTLMRKQAUQJPADHDZPFIKTQBFXAYMVSZPKXIQLOQCVRPKOBZSXIUBAAJBRSNAFDMLLBVSYXISFXQZKQJRIQHOSHVYJXIFUZRMXWJVWHCCYHCXYGRKMKBPWRDBXXRGABQBZRJDVHFPJZUSEBHWAEOGEUQFZEEBDCWNDHIAQDMHKPRVYHQGRDYQIOEOLUBGBSNXWPZCHLDZQBWBEWOCQDBAFGUVHNGCIKXEIZGIZHPJFCTMNNNAUXEVWTWACHOLOLSLTMDRZJZEVKKSSGUUTHVXXODSKTFGRUEIIXVWQYUIPIDBFPGLBYXZTCOQBCAHJYNSGDYLREYBRAKXGKQKWJEKWGAPTHGOMXJDSQKYHMFGOLXBSKVLGNZOAXGVTGXUIVFTGKPJU"

// II I III / 2 11 6 / 1 23 4 / EZ RW MV IU BL PX JO
// "QKRQWUQTZKFXZOMJFOYRHYZWVBXYSIWMMVWBLEBDMWUWBTVHMRFLKSDCCEXIYPAHRMPZIOVBBRVLNHZUPOSYEIPWJTUGYOSLAOXRHKVCHQOSVDTRBPDJEUKSBBXHTTGVHGFICACVGUVOQFAQWBKXZJSQJFZPEVJROJTOESLBQHQTRAAHXVYAUHTNBGIBVCLBLXCYBDMQRTVPYKFFZXNDDPCCJBHQFDKXEEYWPBYQWDXDRDHNIGDXEUJJPVMHUKPCFHLLFERAZHZOHXDGBKOQXKTLDVDCWKAEDHCPHJIWZMMTUAMQENNFCHUIAWCCHNCFYPWUARBBNIEPHGDDKMDQLMSNMTWOHMAUHRHGCUMQPKQRKDVSWVMTYVNFFDDSKIISXONXQHHLIYQSDFHENCMCOMREZQDRPBMRVPQTVRSWZPGLPITRVIBPXXHPRFISZTPUEPLKOTTXNAZMHTJPCHAASFZLEFCEZUTPYBAOSKPZCJCYZOVAPZZVELBLLZEVDCHRMIOYEPFVUGNDLENISXYCHKSJUWVXUSBITDEQTCNKRLSNXMXYZGCUPAWFULTZZSFAHMPXGLLNZRXYJNSKYNQAMZBUGFZJCURWGTQZCTLLOIEKAOISKHAAQFOPFUZIRTLWEVYWMDN"

const int minLength = 100;

Console.WriteLine("\nEnter the message to be decrypted and press > ENTER:");
string ciphertext = Console.ReadLine();

while (!IsInputValid(ciphertext)) {
    Console.ForegroundColor = ConsoleColor.Red;
    Console.WriteLine($"Input is not valid, please enter a message containing only alphabets uppercase letters (minimum length {minLength})");
    Console.ResetColor();
    ciphertext = Console.ReadLine();
}
Console.ForegroundColor = ConsoleColor.Green;
Console.WriteLine("\nInput valid, start decryption...");
Decrypt(ciphertext.ToCharArray());

static bool IsInputValid(string str) {
    if (str.Length < minLength)
        return false;
    Regex r = new("^[A-Z ]+$");
    return r.IsMatch(str);
}

static void Decrypt(char[] ciphertext) {
    FitnessFunction IndexOfCoincidence = new IndexOfCoincidenceFitness();
    FitnessFunction Bigrams = new BigramFitness();
    FitnessFunction Trigrams = new TrigramFitness();
    FitnessFunction Quadgrams = new QuadramFitness();

    Stopwatch stopwatch = new();
    stopwatch.Start();
    Console.ForegroundColor = ConsoleColor.Gray;

    ScoredEnigmaKey rotorConfiguration = ParallelDecryptRotorConfigurations(ciphertext, "", 10, IndexOfCoincidence, AnalysisConstants.AvailableRotors.FIVE);
    //LogCurrentDecryption(rotorConfiguration, ciphertext);

    ScoredEnigmaKey rotorAndRingConfiguration = DecryptRingSettings(rotorConfiguration, ciphertext, Bigrams);
    //LogCurrentDecryption(rotorAndRingConfiguration, ciphertext);

    ScoredEnigmaKey optimalKeyWithPlugs = DecryptPlugboard(rotorAndRingConfiguration, 5, ciphertext, Quadgrams);

    Console.ForegroundColor = ConsoleColor.White;
    LogCurrentDecryption(optimalKeyWithPlugs, ciphertext);
    Console.ResetColor();

    stopwatch.Stop();
    LogTime(stopwatch);
}

/// <summary>
/// Print current decrypted ciphertext
/// </summary>
static void LogCurrentDecryption(EnigmaKey key, char[] ciphertext) => Console.WriteLine($"\nCurrent decryption:\n{new string(new Enigma(key).Encrypt(ciphertext))}\n");

/// <summary>
/// Print current decrypted ciphertext
/// </summary>
static void LogTime(Stopwatch stopwatch) => Console.WriteLine($"\nCurrent time: {stopwatch.ElapsedMilliseconds}\n");

/// <summary>
/// Find the best combination of rotors and start positions (print top n)
/// </summary>
/// <returns> Best enigma settings </returns>
static ScoredEnigmaKey SequentialDecryptRotorConfigurations(char[] ciphertext, string plugboard, int requiredKeys, FitnessFunction f, AnalysisConstants.AvailableRotors rotors) {
    ScoredEnigmaKey[] rotorConfigurations = SequentialAnalysis.FindRotorConfiguration(ciphertext, plugboard, requiredKeys, f, rotors);
    Console.WriteLine("\nTop 10 rotor configurations:");
    foreach (ScoredEnigmaKey key in rotorConfigurations)
        Console.WriteLine($"{key.Rotors[0]}, {key.Rotors[1]}, {key.Rotors[2]}, {key.Indicators[0] + 1}, {key.Indicators[1] + 1}, {key.Indicators[2] + 1}, {key.Score}");
    return rotorConfigurations[0];
}
static ScoredEnigmaKey ParallelDecryptRotorConfigurations(char[] ciphertext, string plugboard, int requiredKeys, FitnessFunction f, AnalysisConstants.AvailableRotors rotors) {
    ScoredEnigmaKey[] rotorConfigurations = ParallelAnalysis.FindRotorConfiguration(ciphertext, plugboard, requiredKeys, f, rotors);
    Console.WriteLine("\nTop 10 rotor configurations:");
    foreach (ScoredEnigmaKey key in rotorConfigurations)
        Console.WriteLine($"{key.Rotors[0]}, {key.Rotors[1]}, {key.Rotors[2]}, {key.Indicators[0] + 1}, {key.Indicators[1] + 1}, {key.Indicators[2] + 1}, {key.Score}");
    return rotorConfigurations[0];
}

/// <summary>
/// Find the best ring settings for the best configuration (index 0)
/// </summary>
/// <returns> Best enigma settings </returns>
static ScoredEnigmaKey DecryptRingSettings(ScoredEnigmaKey key, char[] ciphertext, FitnessFunction f) {
    ScoredEnigmaKey rotorAndRingConfiguration = SequentialAnalysis.FindRingSettings(key, ciphertext, f);
    Console.WriteLine($"Best ring settings: {rotorAndRingConfiguration.Rings[0] + 1}, {rotorAndRingConfiguration.Rings[1] + 1}, {rotorAndRingConfiguration.Rings[2] + 1}");
    return rotorAndRingConfiguration;
}

/// <summary>
/// Perform hill climbing to find Enigma plugs one at a time
/// </summary>
/// <returns> Best enigma settings </returns>
static ScoredEnigmaKey DecryptPlugboard(ScoredEnigmaKey key, int maxPlugs, char[] ciphertext, FitnessFunction f) {
    ScoredEnigmaKey optimalKeyWithPlugs = SequentialAnalysis.FindPlugs(key, maxPlugs, ciphertext, f);
    Console.WriteLine($"Best plugboard: {optimalKeyWithPlugs.Plugboard}");
    return optimalKeyWithPlugs;
}
