[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

# Enigma Cracker

> The Enigma machine is a cipher device developed and used in the early- to mid-20th century to protect commercial, diplomatic, and military communication. It was employed extensively by Nazi Germany during World War II, in all branches of the German military. The Enigma machine was considered so secure that it was used to encipher the most top-secret messages. - Wikipedia

## How it works
The configuration of the Engima cipher machine has three parts:
1. The rotor wheels with overflows and offsets
2. The reflector wheel
3. The plugboard

The heart of the Enigma is the rotor wiring. Each rotor has 26 spring loaded terminals on its right side and 26 flat circular terminals on its left side. The terminals are arranged in circular mode. Each rotor has different fixed internal random fashioned wiring from the left side to the right side of the wheel. Each terminal carries the name of a letter from A to Z (sometimes also numbered from 1 to 26). These letters are arranged on a ring outside and can be seen through the windows of the machine. But the Position of the ring can also be moved in relation to the rotor wiring (ring setting). Each time a letter is pressed, the right wheel moves on one of its 26 places. Once during 26 moves, at the turnover position on right wheel, the middle wheel also moves one place. If the middle wheel reaches turnover position, the last wheel moves, too. The turnover is done by mechanical mechanism using a 'T' shaped pushrod and notches.

![alt text](https://i.stack.imgur.com/ZrGvq.gif)

## Security
There are five rotors of three maybe chosen and inserted into any position. This give us 60 ways of using our rotors. 

5! / (5-3)! = 60 

For each rotor, the relative position of the wiring to the rest of the rotor can be set to 26 positions. This makes an additional 17576 ways of setting our chosen rotors.

26 * 26 * 26 = 17'576 

Each of the 20 extremities of the 10 wires can be plugged into any of 26 positions not otherwise occupied, with the 2 extremities of a given wire equivalent, as well as the 10 wires themselves.

26! / [(26 - 20)! * 2^10 * 10!] = 150'738'274'937'250 

Resulting in:

60 * 17'576 * 150'738'274'937'250 = **1,6 * 10^20**

possible combinations. This number does not account for *the position of the r rotors*, which before actual encryption of a message is determined by the setup procedure, and is initially unknown to the cryptanalyst. Accounting for that, there are thus **1,1 * 10^23 states of the Enigma hardware** when encryption begins.

## Attack explanation
The device wiring does not allow that a letter enciphers to it's self. A 'P' will never become a 'P'. And this was one of the main weak sides of the Enigma machine. During the war this not-self encryption property and the assumption of small portion of text (for example, assuming that the last two words of an encrypted message were always “Hail Hitler”) allow the british electro-mechanical device to easily cycle through all possible combinations within 20 minutes to solve the code. This is called a **plaintext attack**.

More than 70 years later my approach follow the one originally proposed by [James J. Gillogly](https://web.archive.org/web/20060720040135/http://members.fortunecity.com/jpeschel/gillog1.htm) and [Mike Pound](https://github.com/mikepound/enigma). This concept is to try to solve Enigma by recovering the message key settings, the ring settings, and the plug settings individually through a brute force attack and without decripted text assumptions. This is called a **ciphertext-only attack**. We create the class of an Enigma machine and brute force each possible combinations of rotors (for example) and analyze the output. The simulated device will decrypt it, probably it will decrypt it wrong but when we accidently stumble upon the right rotor configuration we will find that the decryption will be slightly better then the others configurations. This procedure is sensitive enough to distinguish the correct rotor order. The attack uses different **fitness functions** to measure the effectiveness of a test decryption. 

#### Index Of Coincidence
The Index Of Coincidence or IoC is the probability that, when you pick two letters at random, they'll be the same.
The program will go through and count every single character and how many of each one there are. Then through the histogram produced you can calculate the IoT. Usually this value for random, evenly distributed text is about 0.038. Most modern languages plain text transcription instead will result in an IoT significantly higher, about 0.067 for english, 0.072 for german. Since the possible combinations are **1,6 * 10^20** we can't go through each possible settings, but we can go through some of the settings at a time using a modern pc computational power.

This type of fitness function (and also other ones like bigrams, trigrams..), however, has a hidden flaw. The processed characters needs to be enough to start outputting valuable scoring. This occur because even if some of the letters start to appear right there is not a lot of them, so the IoC will be just noise. For this reason small messages, under 50 characters are really difficult to decrypt using this method, which starts giving excellent results with more than 150 characters and under 10 plugs. For this reason the messages during the war were limited to 200 characters. Bigrams, trigrams and quadrams fitness functions are also implemented and they give better results for the plugboard decryptions. 

## License
MIT License

Copyright (c) [year] [fullname]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
